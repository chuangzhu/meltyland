# 运算放大器

稍微庆祝一下我找到了失踪了两年的笔记本 🎉️，这篇笔记就是从我的这个笔记本上扒下来的。

![Op Amp](https://upload.wikimedia.org/wikipedia/commons/9/97/Op-amp_symbol.svg)

这是一个运算放大器，它是一种模电的设备。V<sub>S+</sub> 和 V<sub>S-</sub> 是运算放大器的电源接口，实际应用时会用到，但是在制作原理图时为了方便常常会省略画出。V<sub>+</sub> 和 V<sub>-</sub> 之间是几乎绝缘的，运算放大器最直接的作用就是感知 V<sub>+</sub> 和 V<sub>-</sub> 之间的电压差，然后放大。放得非常大：
$$
V_{OUT} = \begin{cases}
a (V_+ - V_-), a(V_+ - V_-) \lt V_S\\\\
V_S, a(V_+ - V_-) \geq V_S
\end{cases}
$$
其中 a 就是放大倍数，我们常常只知道这是一个很大的数，它的数值可能是几百万，但我们常常不知道他的具体值。不同厂商生成的不同型号的运算放大器的放大倍数会有不同，甚至于同一个运算放大器的放大倍数也极其不稳定、易受温度影响。

那么如何减少运放受温度的影响并控制它的放大倍数呢？

## 同相放大器电路

![Non-inverting Op Amp](https://upload.wikimedia.org/wikipedia/commons/6/66/Operational_amplifier_noninverting.svg)

（这里我们省略了电源接口，并假设电源可以提供足够高的电压。）

首先运算放大器会将 V<sub>+</sub> 和 V<sub>-</sub> 之间的电压放大：
$$
V_{OUT} = a (V_+ - V_-)
$$
其中 V<sub>+</sub> 就是 V<sub>IN</sub>。而 V<sub>-</sub> 接在 R<sub>f</sub> 和 R<sub>g</sub> 分压的 V<sub>OUT</sub> 和 GND 之间，所以：
$$
V_- = V_{OUT} \cdot \frac{R_g}{R_f + R_g}
$$
代入：
$$
V_{OUT} = a (V_{IN} - \frac{V_{OUT} \cdot R_g}{R_f + R_g})
$$
左边是 V<sub>OUT</sub>，右边也有 V<sub>OUT</sub>，我们本能地变形一下式子：
$$
V_{OUT}(1 + \frac{a \cdot R_g}{R_f + R_g}) = a \cdot V_{IN}
$$

$$
V_{OUT} = \frac{a \cdot V_{IN}}{1 + \frac{a \cdot R_g}{R_f + R_g}}
$$

因为 a 是个很大的数，所以 $\frac{a \cdot R_g}{R_f + R_g}$ 也一定是个很大的数。试想 1 块钱加上几百万，那么这一块钱是完全可以忽略掉的。这样我们化简式子就变得很方便了：
$$
V_{OUT} = \frac{a \cdot V_{IN}}{\frac{a \cdot R_g}{R_f + R_g}} = \frac{V_{IN}}{\frac{R_g}{R_f + R_g}} = V_{IN} \cdot \frac{R_f + R_g}{R_g}
$$
很幸运，最后化简的结果只包含 V<sub>IN</sub> 这一个变量，剩下的常量 R<sub>f</sub> 和 R<sub>g</sub> 都是我们可以控制的。比如我想让输出的电压是输入电压的十倍，我只需要让 R<sub>f</sub> = 9kΩ，让 R<sub>g</sub> = 1kΩ 就可以做到。

是不是很神奇，两个小小的电阻怎么就让这样极其不稳定的运算放大器乖乖听话的？我们结合电路，重新审视一下这个式子：

![Non-inverting Op Amp](https://upload.wikimedia.org/wikipedia/commons/6/66/Operational_amplifier_noninverting.svg)
$$
V_{OUT} = a (V_{IN} - \frac{V_{OUT} \cdot R_g}{R_f + R_g})
$$
我们可以看到，输出的电压的一部分被当作输入电压，和真正的 V<sub>IN</sub> 一起在调节着 V<sub>OUT</sub>。当 a 因为温度升高而增大时，如果 V<sub>OUT</sub> 跟着增大，那么 V<sub>-</sub> 的输入也会增大，V<sub>+</sub> 和 V<sub>-</sub> 之间的差值此时就会减小。最后又作用到 V<sub>OUT</sub> 上，迫使 V<sub>OUT</sub> 跟着减小。这其实就是负反馈调节，当一个因素偏离正常值时，就会引发其他的因素将它拉回正轨。

除了同相放大器电路以外还有一些其他的放大器电路：

## 反相放大器电路

![Inverting Op Amp](https://upload.wikimedia.org/wikipedia/commons/2/2d/Opampinverting.svg)
$$
V_{OUT} = - \frac{R_f}{R_{in}} \cdot V_{IN}
$$

## 后记

分享一下 MIT 的电路与电子学公开课 [`av5094941`](http://www.bilibili.com/video/av5094941)，虽然是很古老的视频了，但还是很有趣的。
