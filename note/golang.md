```page.ini
created = 1539148260
modified = 1545055637
```

# Go 语言

## func 函数

```go
type MType struct{}
func (mtype MType) name(args T) ([ret_var] T)
```

最后的是返回的类型，这里可以指定返回变量的名称，这样只用一个 `return` 就可以返回对应名称的变量。

## map

类似于其他语言的词典。

```go
func name(m map[string]int) {}
m := make(map[string]int)
m["first"] = 123
m["second"] = 666
value, ok := m["third"]
// ok: fasle
```

## interface 接口

在 `interface` 中指定几个函数作为一个类型，{{ 定义了 { 这些函数 } 的类型 } 的实例变量 } 就被称为 “满足这个接口”，可以赋值给这个接口。

`interface{}` 为空接口，任何类型都实现了它，所以什么都能往里面塞（包括 map, struct），类似 ts 中的 any。

需要注意的是 interface 本身不是那个类型。比如说如果把一个 map 赋值个一个接口，那么你不能给变量赋值。

可以将 interface 转换为 map，然后键值赋值。
map, struct 不可以转换为 interface。

## package 包

只有 main 包才能运行，其他名称都是作为库使用的。

## new(T) make(T)

make 只能用于 map, slice, channel，返回一个初始化的量。new 则返回初始化量的指针

## 指针

> https://blog.csdn.net/cyk2396/article/details/78893828

golang 中绝大多数类型都是直接传值：

基本类型: byte, int, bool, string

复合类型: array, slice, struct, map, channnel