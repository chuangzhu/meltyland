```page.ini
created = 1543123565
modified = 1545546189
```

![print.jpg](https://i.loli.net/2018/12/15/5c14f8c676b5c.jpg)

# Linux 下使用网络打印机

> https://wiki.archlinux.org/index.php/CUPS#Network
>
> https://wiki.archlinux.org/index.php/Avahi#Hostname_resolution
>
> 特别注意要按照 wiki 逐步操作，不然很容易乱。

CUPS 是 Linux 下主流的打印服务框架，安装 `cups`。

Avahi（zeroconf）可用于查找本地网络中的打印机，安装 `avahi`。然后编辑 `/etc/nsswitch.conf`，将 `hosts` 那一行改为：

```
hosts: files mymachines myhostname mdns_minimal [NOTFOUND=return] resolve [!UNAVAIL=return] dns
```

然后启动服务：

```sh
systemctl start avahi-deamon.service
systemctl restart org.cups.cupsd.service
```

## 打印机驱动

Foomatic 提供了很多打印机驱动（pdd 文件，又称 model），首先安装 `foomatic-db-engine`，然后安装 `foomatic-db`、`foomatic-db-ppds`、`foomatic-db-nonfree-ppds`。惠普打印机有特殊的驱动，安装 `hplip`。

### 获取驱动路径

```sh
lpinfo -m | less
```

在里面找到你的打印机型号，比如我找到了：

```
drv:///hp/hpcups.drv/hp-deskjet_2600_series.ppd HP Deskjet 2600 Series, hpcups 3.18.6
lsb/usr/HP/hp-deskjet_2600_series.ppd.gz HP Deskjet 2600 Series, hpcups 3.18.6
```

那么驱动的路径就是 `drv:///hp/hpcups.drv/hp-deskjet_2600_series.ppd`。

## 获取打印机 URI

```sh
sudo lpinfo -v
```

输出：

```
...
network socket://192.168.1.104:9100
```

那么 `socket://192.168.1.104:9100` 就是打印机的 URI。

## 添加打印机

```sh
sudo lpadmin -p <给打印机取个名> -E -v <URI> -m <驱动路径>
```

打开 Chromium，<kbd>ctrl</kbd> <kbd>p</kbd> 选择打印机就可以打印了。可以设定默认打印机，使用 `lp` 命令打印文件。