```page.ini
created = 1539359664
modified = 1540658110
```

### Map

利用一个函数，使一个数组每一项都映射在另一项上，产生一个新的数组。

```JavaScript
function sqr(x){
    return x*x;
}
var arr=[0, 1, 2, 3, 4];
var res=arr.map(sqr);
/* res: [0, 1, 4, 9, 16] */
```

### Reduce

利用一个两个参数的函数，将前两项的运算结果，作为第一个参数与后一项进入函数运算。

`[x0, x1, x2, x3].reduce(f)=f(f(f(x0, x1), x2), x3)`

```JavaScript
function add(x, y){
    return x+y;
}
var arr=[0, 1, 2, 3, 4];
var res=arr.reduce(add);
/* res: 10 */
```

### Filter

用于筛选数组元素的高阶函数，返回为 false 则筛掉，反之则保留。

```JavaScript
var arr=["a", "b", "a", "c"];
var res=arr.filter(function (element, index, self){
    /* 筛掉重复的元素 */
    return self.indexOf(element)===index;
});
```

### Sort

用于排序的高阶函数，如果没有输入函数按默认方式排序。输入函数将比较两个参数，返回 -1 表示前一项应排在后一项之前，返回 1 表示前一项应排在后一项之后，返回 0 则表示两项并列排序。

```JavaScript
var arr=[2, 10, 40, 3]
/* 调用该方法会改变当前数组 */
arr.sort(function (x, y){
    if(x<y) return -1;
    if(x>y) return 1;
    return 0;
});
```

