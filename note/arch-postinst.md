```page.ini
created = 1537106106
modified = 1540658110
```

# Arch 成功启动后的配置

前一篇：[安装 archlinux](/note/arch-install)

既可以启动安装的 Arch 操作，也可以在 archiso 中 `chroot` 操作。

## 时区

> [Time - ArchWiki](https://wiki.archlinux.org/index.php/time#UTC_in_Windows)

首先要修改硬件时钟为 UTC 时区。Windows 默认使用当地时间，修改后如果要让 Windows 时区正常，要设置 Windows 注册表。

```sh
ln -sf /usr/share/zoneinfo/Asia/Chongqing /etc/localtime
hwclock --systohc
```

## 主机名

```sh
vim /etc/hostname
vim /etc/hosts
```

```
127.0.0.1	localhost
::1		localhost
127.0.1.1	主机名.localdomain	主机名
```

## 本地化 locale

```sh
vim /etc/locale.gen
```

取消注释 `zh_CN.UTF-8 UTF-8` `zh_HK.UTF-8 UTF-8` `zh_TW.UTF-8 UTF-8` `en_US.UTF-8 UTF-8` 。然后：

```sh
locale-gen
```

把 `/etc/locale.conf` 的内容改为：

```
LANG=en_US.UTF-8
```

## 配置网络

首先测试一下：

```sh
ping github.com
```

要使用网络，在光盘系统中 chroot，安装：

```sh
pacman -S dialog wpa_supplicant networkmanager
```

要启用有线网络连接，使用 `dhcpc`。

启用无线网络连接，使用 `wifi-menu`。
