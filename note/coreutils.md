```page.ini
created = 1539840299
```

# GNU/Linux 基础组件

## 文件管理

### touch

创建文件，如果文件存在则更新它的更改时间。

### mkdir

创建目录，使用 `-p` 参数可以直接带着父目录一起创建。

### mv

```sh
mv <source> <dest>
```

移动文件，同设备移动是先 hard link 然后 unlink，速度很快；跨设备则是复制后删除。

* 如果 `<dest>` 不存在，则会将 `<source>` 移动到该位置。
* 如果 `<dest>` 存在，
  - 若 `<dest>` 的位置是个文件，则该文件将被覆盖。
  - 若 `<dest>` 是目录，则会将 `<source>` 移动到 `<dest>` 目录下的同名文件。

### cp

```sh
cp [-r] <source> <dest>
```

复制文件。`-r`: recursive，复制目录时需指定该参数。

### rm

删除文件。

* `-r`: 递归删除，删除非空的目录是需指定该参数。
* `-f`: 强制删除。包括你拥有的路径下的其他用户创建的文件，或是你拥有的、但是没有写入权限的文件。
* `-i`: 每个删除操作都提醒。

### ls

显示当前路径下文件。

* `-l` 列出文件的一些元信息
* `-a` 显示带点的文件

### cat

```sh
# 读取文件输出至 stdout
cat file.txt

# 将 stdin 输出至 stdout
echo 2333 | cat

# 一种常见的将一段内容写入文件的方法
cat > file.txt << EOF
Here
    You Can Even
                Write A Poem
EOF
```

### stat

查看文件元数据。

### basename

```
basename <path> [<suffix>]
```

去除文件路径的目录。

* `<suffix>`: 同时去除指定后缀

### dirname

去除文件路径的文件名。

### readlink

```sh
readlink <symbolic-link>
```

读取软链接的指向。

```sh
readlink -f <file>
```

获取任意文件的完整路径。

### find

遍历搜索文件，找到后还可以指定一些操作（默认是直接打印出来）。

```sh
find [dir] [-type <type>] [-name <pattern>] [-perm <mod>] [-regex <pattern>] [-print] [-delete] [-exec <command> \;]
```

* `-type <type>`: 匹配指定文件类型。`<type>`: `f`（普通文件）, `d`（路径）, `l`（软链接）, `p`（FIFO）, `s`（socket）。
* `-name <pattern>`: 匹配指定文件名，可以用通配符（shell glob）。
* `-perm <mod>`: 匹配指定文件权限。
* `-regex <pattern>`: 与 `-name` 类似，但用的是正则表达式。
* `-print`: 输出匹配到的文件（默认）。
* `-delete`: 删除匹配到的文件。
* `-exec <command> \;`: 对匹配到文件时，运行程序。`<command>` 中可以用 `{}` 代替文件名。这个参数的行为是每匹配到一个文件就会执行一次程序，因此在文件较多时可能会比较慢。对于一些同时指定多个文件的程序（`cp`, `mv`, `rm`），建议用 `find -print | xargs <command>`。

### chmod

改文件模式。

```sh
chmod [ugo][-+=][rwxXst] <file>...
```

* `u` 为 owner 设置文件模式
* `g` 为 group 设置文件模式
* `o` 为 others 设置文件模式
* `-` 移除一个模式
* `+` 添加一个模式
* `r` 可读
* `w` 可写
* `x` 可执行

```sh
chmod <mode> <file>...
```

* `<mode>`: 八进制文件模式。

### chown

```sh
chown <user> <file>
chown <user>:<group> <file>
```

更改文件所有者。

### chgrp

```sh
chgrp <group> <file>
```

更改文件所有组。

### tar

```sh
tar c|x [-zJjO] [-f <target>]
```

* `c` 打包
* `x` 解包
* `-z` 打包后使用 gzip 压缩
* `-J` 使用 xz 压缩
* `-j` 使用 bzip2 压缩
* `-f <target>` 从文件解包/打包至文件。否则从 stdin 解包/打包至 stdout。
   * `-O` 解包至 stdout。否则解包至当前路径。

### sed

一个远古时期人们用的命令式编辑器。现在一般只用来替换文件中的内容。

```sh
sed [-i] <command> [-e <command>] [file]
```

* `<command>` 的一般形式是 `[line]s/<pattern>/<replace-with>/g`。
  * `line`: 在第几行替换，未指定就是全文。
  * `s/`: 查找并替换命令。后面的 `/` 是分隔符号。一些时候你的 `<pattern>` 或 `<replace-with>` 中可能会包含挺多分隔符本身，转义起来比较麻烦。这时候就可以指定其他符号作为分隔符。如 `s/melty.land\/blog\/coreutils/melty.land\/note\/coreutils/g` -> `s|melty.land/blog/coreutils|melty.land/note/coreutils`。
  * `<pattern>`: 用于匹配的正则表达式，可用 `\(\)` 包住其中的一部分，以便在 `<replace-with>` 中引用。
  * `<replace-with>`: 替换为。可用 `\1`, `\2`, etc. 代替 `<pattern>` 中匹配到的用 `\(\)` 包住的内容。
* `-i`: inplace。默认行为是将替换后的内容输出到 stdout，使用这个参数将会输出到被编辑的文件里。
* `-e <command>`: 使用这个参数你可以在一条 sed 中按照顺序进行多次编辑。

### df

显示当前挂载的文件系统的使用状态。

* `-h` 人类可读，即自动设置显示单位
* `-x <fs>` 不包含指定类型的文件系统
* `-B<unit>` 手动设置显示的单位。`<unit>`: `K`, `M`, `G`, 都是以 2 为底。

### du

显示当前路径下的路径占空间大小。

* `-a` 显示文件大小
* `-h, --human-readable` 显示单位
* `-d N, --max-depth N` 显示的深度
* `--exclude=A` 不包含名为 A 的文件。

### dd

```sh
dd if=/path/to/a.iso of=/dev/sdb [bs=<size>] [count=<times>] [status=progress]
```

* `status=progress` 显示进度。如果运行前忘记指定，也可以在运行时向该进程发送 `SIGUSR1` 显示。

### ln

```sh
ln [-sf] <source> <link>
```

创建链接，`-s`: 符号链接，`-f`: 创建链接时可覆盖链接所在位置的文件。

### shred

一些存储媒介上的数据无法轻易抹除，shred 可以对一个文件的位置反复擦写，降低恢复的可能性。

## I/O

### echo

打印 arg 中传入的内容。默认是原样输出，使用 `-e` 可以让 `echo` 解析 arg 中的转义字符。

### printf

和 C 中的 `printf` 类似。除了可以格式化输出以外，还可以当作输出不会换行的 `echo` 用。

### less & more

```sh
less [-R] [file]
```

用于浏览过长的文件，分步输出文件内容。若未指定文件则从 stdin 读取。

* `-R` 不转义输出颜色代码，而是带颜色输出。

操作：

* `j k` 上下
* `q` 退出
* `/string` 查找
* `n N` 查找下一项/上一项
* `?string` 反向查找
* `g` 头
* `G` 尾

more 和 less 很像，功能比 less 弱一些。

### sort

```sh
sort [-hnrz] [<file>]
```

以换行为分隔，排序文件中的行并输出到 stdout。如果未指定 `<file>` 则排序 stdin 读入的内容。

* `-h`: 按储存中的单位排序。e.g. `1G 5K 10M 0.2T` -> `5K 10M 1G 0.2T`。
* `-n`: 按数字大小排序。
* `-r`: 逆向排序。
* `-z`: 以 `\0` 为分隔符，在脚本中比较有用。

### head

```sh
head [-n <lines>] <file>
```

查看文件的前 `<lines>` 行，未指定则是前 10 行。

### tail

```sh
tail [-n <lines>] [-f] <file>
```

查看文件的后 `<lines>` 行，未指定则是后 10 行。

* `-f`: 定时重新读取文件，输出文件新增的内容。可用来看 log。

### grep

```sh
grep "<regexp>" [<files>]...
```

在 files 或者 stdin 中匹配正则表达式，输出这些行

```
grep -r "<regexp>" [<dirs>...]
```

递归模式，在路径下查找匹配

### cut

```sh
cut [-d<delimiter>] -f <part>
```

以 `<delimiter>` 为分隔符分割 arg（未指定时默认以 tab 为分隔符），然后输出第 `<part>` 部分（从 1 开始计数）。

### wc

```sh
wc [-cwl] [file]
```

计数文件（未指定为 stdin）的字符数（`-c`），词数（`-w`）或行数（`-l`）。

### tee

从 stdin 读取，输出到指定文件和 stdout。

## 进程

### ps

列出进程。默认只会列出父进程和父进程的子进程。有两类指定参数的风格，Unix style & BSD style。

#### Unix style

* `-A` 列出所有进程
* `-u <user>` 列出指定用户的进程

#### BSD style

`aux` 列出所有进程

### kill

```sh
kill [-SIG<signal>] [-<signal-number>] <process>
```

向进程发送信号，如果未指定信号，则发送 `SIGTERM`。`<signal>` 和 `<signal-number>` 可用 `kill -l` 列出。`-9` 就是 `SIGKILL`，会强行终止进程。

## 其他

### man

用户手册。

```sh
man [section] <page>
```

### \[

~~没错它是个程序。~~常在 shell 的 if 语句中使用的用于判断文件是否存在、arg 是否为空的程序。

### true

返回 exit code `0`。

### false

返回 exit code `1`。

### pwd

打印工作路径。

### env

```sh
env [<environ-var>=<value>] <program> [<args>...]
```

在 `PATH` 环境变量中寻找 `<program>`，然后在更改了环境变量 `<environ-var>` 为 `<value>` 的环境中运行 `<program> [<args>...]`

### mkfifo

创建一个 FIFO (named pipe)。

### mktemp

创建一个临时目录（在 `/tmp` 下），并返回目录名称。

### uname

打印内核名称。

### nproc

打印 CPU 核心数。

### md5sum/sha1sum/sha224sum/sha256sum/sha384sum/sha512sum

计算文件的 checksum，未指定文件则从 stdin 读入。

### base32/base64

```sh
base64 [-d] [-w <columns>] [<file>]
```

编码/解码 base32/base64。`<file>` 未指定则为 stdin。`-d`: 解码。默认会换行输出，这在脚本中使用时会导致一些问题，可用 `-w 0` 不换行输出。

