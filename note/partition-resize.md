```page.ini
created = 1541780743
modified = 1541780743
```

# Linux 下调整分区大小

背景：Arch Linux 使用一段时间后觉得很好，但是空间不够用了。于是删掉了原本安装的 Ubuntu 准备扩大分区。这里的 Ubuntu 在硬盘上的扇区位置在 Arch Linux **之后**，可以通过这种方式调整分区。如果你删除的系统在硬盘上的位置在你想要扩容的系统**之前**的话，就只能备份后格式化掉（也就是删除分区、重新建立分区、重新建立文件系统）原来的分区，然后恢复备份了。

> [How to Resize a Partition using fdisk](https://access.redhat.com/articles/1190213)
>
> [How to Shrink an ext2/3/4 File system with resize2fs](https://access.redhat.com/articles/1196333)

分为两步，调整分区大小和调整文件系统大小。

## 调整分区

调整分区大小只能通过删除分区然后新建分区完成。放心，删除分区不等于抹除数据，只要操作正确，数据是不会丢失的。

系统不能对自己所在的分区进行操作，又到 live CD 发挥作用的时候了。

```sh
$ fdisk /dev/sda

Command (m for help): p
Device         Start       End   Sectors   Size Type
/dev/sda1       2048   1026047   1024000   500M EFI System
/dev/sda2    1026048       ...       ...   30G Linux filesystem

Command (m for help): d
Partition number (1,2, default 2): 2

Command (m for help): n
Using default response p. 
Partition number (2-4, default 2):
First sector (1026048-854745087, default 1026048): # ATTENTION 1
Last sector (1026048-854745087, default 854745087):
Partition #2 contains a ext4 signature.
Do you want to remove the signature? [Y]es/[N]o: n # ATTENTION 2

Command (m for help): w # ATTENTION 3
```

* **ATTENTION 1**: first sector 要和之前的 start 对上，否则会导致数据损坏。
* **ATTENTION 2**: 不要移除签名，否则要重新生成 fstab。
* **ATTENTION 3**: 直到这一步才真正作出更改，所以在这之前要仔细检查。

## 调整文件系统

```sh
$ e2fsck -f /dev/sda2
$ resize2fs /dev/sda2 [size]
```

如果不指定 size 的话，`resize2fs` 默认调整文件系统到分区大小。