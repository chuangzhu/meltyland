```page.ini
created = 1545546189
modified = 1545546189
```

# 前端的 npm

Node 早已不只是后端的 JavaScript 运行时，很多 npm 包成为了前端的工具、前端框架和前端库。现在前端应用越来越复杂，传统的前端编写方式已经满足不了大家的需求，人们希望把后端构建应用的方式带到前端。

Node 环境与浏览器环境存在很大的不同，如浏览器没有 `require` `module`。要想让浏览器使用~~西方~~ node 那一套，有两种方式：

* 加载 require.js 等的 `<script>` 标签，使得浏览器支持 `require` `module`，相当于一个在线 `require` 解释器。
* 在工作电脑上安装前端库、编写前端程序，然后使用一些工具把这些程序连同库一起转化为浏览器可以用的 js 文件（`bundle.js`），再把这些作为静态文件进行 deploy。相当于在工作电脑上 “编译” 程序，然后放到服务器上。

这样（第二种）的主流工具有 `browserify`、`webpack`，当然这些工具也是 npm 包，要在工作电脑上和前端库 npm 包一同安装。

> [Getting Started - Material Components for the Web](https://material.io/develop/web/docs/getting-started/) 通过 npm 安装 material design

`browserify` 实现的功能比较简单。

`webpack` 除了转化 npm 包，还能加载其他编译器（叫做 `loader`），将 TypeScript 编译为 js、将 Sass/less 编译为 CSS、将 ES6 编译为 ES5 等等。

上面那个链接是一个实例，过一遍就大概了解 `webpack` 是什么了。