```page.ini
created = 1541919946
modified = 1545055637
```

* pi 0 w
* tf memory card 32g c10
* gnu/linux

> [树莓派3B+ 安装系统 - 宁静致远's 博客 - CSDN博客](https://blog.csdn.net/kxwinxp/article/details/78370913)

## 获取 raspbian

[Download Raspbian for Raspberry Pi](https://www.raspberrypi.org/downloads/raspbian/)

有两个版本，有 lxde 的 RASPBIAN STRETCH WITH DESKTOP 和 RASPBIAN STRETCH LITE。

```sh
curl -fLO https://downloads.raspberrypi.org/raspbian_lite_latest
sha256sum raspbian_lite_latest
unzip raspbian_lite_latest
umount /dev/mmcblk0*
sudo dd if=2018-10-09-raspbian-stretch-lite.img of=/dev/mmcblk0
```

最后一行是整个 tf 卡，注意不要选成分区了。

## 开机前配置

首先挂载 `/dev/mmcblk0p*`。

### 开启 SSH

在 boot 分区中创建一个空文件 `ssh`。

### 设置 wifi

`boot/wpa_supplicant.conf`：

```ini
country=CN
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="WIFI NAME"
    psk="passwd"
    priority=1
}
```

启动后它将被复制至 `/etc/wpa_supplicant/wpa_supplicant.conf`。

### 连接

![router](https://i.loli.net/2018/11/10/5be65d3fc78f3.png)

```sh
ssh pi@192.168.1.100
```

![ssh](https://i.loli.net/2018/11/10/5be65dace5304.png)

如果该 ip 被你连接过的其他设备使用过，那么你可能需要编辑 `~/.ssh/known_hosts`，删掉那个指纹。

## raspbian 镜像

http://mirrors.ustc.edu.cn/help/raspbian.html

http://mirrors.ustc.edu.cn/help/archive.raspberrypi.org.html