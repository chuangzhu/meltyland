```page.ini
created = 1539840299
```

这篇笔记记录一些不属于 GNU Coreutils 但是比较常用的 Linux CLI 软件的简单用法。

## 网络

### curl

下载文件。

```sh
curl [-fsSLO46] [-o <file>] [-m <timeout>] <url>
```

* `-f` 页面未找到（`404`）时输出错误。
* `-s` 不显示进度和错误信息。
* `-S` 和 `-s` 一起用，不显示进度但显示错误。
* `-L` 允许重定向。
* `-o <file>` 保存到指定文件而不是输出到终端。
* `-O` 按照 URL 保存到文件。
* `-m <timeout>` 超时时长。
* `-4`/`-6`: 将域名解析为 IPv4 还是 IPv6。

#### 高级用法

cURL 除了可以用来下载文件，更是一个能发送各种 HTTP 请求的工具。

```sh
curl [-X<method>] [-H '<header>: <value>'] [-b '<cookie>=<value>'] [-c <file>] <url>
```

* `-X<method>`: HTTP 请求方法。`<method>`: `GET`, `POST`, `PUT`, `DELETE`, `HEAD`
* `-H '<header>: <value>'`: 设置 HTTP 头。
* `-b '<cookie>=<value>'`: 设置 cookie。
* `-c <file>`: 将 cookie 储存在网景 cookie jar 类型文件 `<file>` 中。

#### POST

当这些参数被设置时，cURL 会自动选择使用 `POST` 方法，并设置 `Content-Type`。

```sh
curl [-F '<field>=<content>'] [-d '<field>=<content>'] [-b <body>] <url>
```

* `-F '<field>=<content>'`: 以 `multipart/form-data` 格式发送 `POST` 请求。`<content>` 中可以使用 `@<file>` 来指定读取文件内容发送。
* `-d '<field>=<content>'`: 以 `application/x-www-form-url-encoded` 格式发送 `POST` 请求。
* `-b <body>`: 直接发送该内容。

### wget

```sh
wget [-mq] [-o <file>] [-T <timeout>] <url>
```

* `-m`: 递归地爬取网站页面，可用于归档网站。
* `-q`: 安静。
* `-o <file>`: 保存到指定文件。默认按照 URL 保存文件，使用 `-o -` 输出到终端。
* `-T <timeout>`: 超时时长。

### dig

DNS lookup utilty.

```sh
dig [@<NS>] <domain> [+short] [<type>]
```

* `@<NS>`: 查询的 DNS 服务器
* `+short`: 仅输出记录内容
* `<type>`: `TXT`, `NS`, `A`, `AAAA`, `CNAME`, `CAA`, `AXFR`

### nmap

网络探测以及网络安全检测。

```sh
nmap [-p<start>-<end>] <cidr>
```

* `-p<start>-<end>`: 端口范围

### proxychains

将代理用到任意程序上。用法：

```sh
proxychains <program> [args]
```

配置文件在 `/etc/proxychains.conf`。

## 交互

### tmux

tmux 是一个 shell host，使用 tmux 可以复用同一个 shell。正常情况下是 shell -> terminal，有了 tmux 后是 shell -> tmux -> terminal。

`tmux detach` 离开这个 shell（但没有终止）。`tmux attach` 继续那个 shell。

配置文件（rc）在 `~/.tmux.conf`。

由于 apt 升级过程中会出现很多 ncurse TUIs 需要手动作出选择，所以我喜欢在 tmux 中升级。这样万一 ssh 断开后出现一个 TUI，我还能 attach 上那个 session 继续操作。

## 包管理器

### pacman 

* `-S <package>` 安装包

* `-S`
   * `-s <package>` 搜索包
   * `-i <package>` 显示包信息
   * `-y` 更新源
      * `-y` 强制更新源
   * `-u` 更新所有包（更新系统）
   
* `-R <package>` 移除包（不要使用）

* `-R`
  
   * `-s  <package>` 移除包和它依赖而其他程序不依赖的包
   * `-c <package>` 移除包和依赖它的包
   * `-n <package>` 同时删除包中在文件系统上被修改的文件（e.g. 配置文件）
   
   我一般直接用 `-Rsunc <package>`。
   
* `-Q [<package>]` 查找已安装的包

* `-Q`

   * `-l <package>` 列出一个包的内容
   * `-o <file>` 列出哪个包拥有这个文件
   * `-m` 列出不是从源中安装的包（AUR 以及你下载的 `.pkg.tar.xz` 包）
   * `-q` 安静点

### apt

* `install <package>` 安装
* `search <package>` 搜索包
* `list [<package>]` 列出源中包，未指定则为所有包
   * `--installed` 已安装的
   * `--upgradable` 可更新的
* `show` 显示包详细信息
* `update` 更新源
* `upgrade [<package>]` 更新包，如果未指定则更新所有包

### dpkg

* `-L <package>` 列出包内容
* `-S <file>` 搜索包含某文件的包

