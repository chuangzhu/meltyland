```page.ini
created = 1537031064
modified = 1549991577
```

# 安装 archlinux

> [Installation guide - ArchWiki](https://wiki.archlinux.org/index.php/Installation_guide) 
>
> [以官方Wiki的方式安装ArchLinux | viseator's blog](https://www.viseator.com/2017/05/17/arch_install/)

## 思路

[刻盘](#刻盘) -> [分区](#分区)（partitioning）-> [格式化](建立文件系统)（建立文件系统）-> [安装系统](安装系统) -> [安装 GRUB](#安装-grub) -> 配置

## 刻盘

> [Linux命令行烧录树莓派镜像至SD卡](https://blog.csdn.net/qq_19175749/article/details/52700246)

下载 iso 后最好校检一下，使用 PGP 当然是最好的，简单的话 `md5sum` 一下就可以了。

使用一台安装了 GNU/Linux 机器刻盘是一件很容易的事。

`dd` 被描述为一个复制和转换文件的工具。而在 \*nix 下一切皆文件，设备就是 “特殊的文件”，`dd` 可以用于刻盘。

```sh
ls /dev/sd* # 在其中找到光驱或 usb 闪存盘设备
# 假如 usb 闪存盘为 /dev/sdb
umount /dev/sdb*
# if: input file, of: output file
sudo dd if=/path/to/archlinux-x86_64.iso of=/dev/sdb
```

## 检查引导类型

引导类型和主板有关，一种主板引导类型对应着一种硬盘分区表。_BIOS（又称 Legacy）_ 引导的机器使用 _MBR（又称 MSDOS）_分区表；_EFI（又称 UEFI）_引导的机器使用 _GPT_ 分区表。

```sh
ls /sys/firmware/efi/efivars
```

如果显示没有这个路径，则为 BIOS/MBR。否则为 EFI/GPT。

* 我的 Dell 电脑同时支持 BIOS 和 EFI 引导，硬盘装机分区表是 GPT。
* Mac 使用的是 EFI/GPT。
* Virtualbox 主要是 BIOS/MBR。
* Virtualbox 创建虚拟机时，选择 Mac 可以创建 EFI/GPT 虚拟机。
* [Chapter 3. Configuring virtual machines](https://www.virtualbox.org/manual/ch03.html#efi)

进入光盘系统后。分区工具主要有两个：`fdisk` 和 `parted`

## 分区

> [Patitioning - ArchWiki](https://wiki.archlinux.org/index.php/Partitioning)

可以使用 `fdisk -l` 查看分区状态。如果显示不完整，提醒 `Partition table entries are not in disk order.`，可能有几种原因：[How To Fix if Partition Table Entries not in Disk Order](https://linoxide.com/how-tos/partition-table-entries-disk-order/) ，不影响操作。

### 空硬盘

#### MBR/BIOS

MBR 不需要单独立一个引导分区。选择以下其中一个工具操作：

```sh
$ parted /dev/sda
(parted) mklabel msdos # 又称 MBR
# 如果空间足够的话还可以分一个交换区
# mkpart primary linux-swap 1 1.1G
(parted) mkpart primary ext4 100M -1 # 根路径
```

```sh
$ fdisk /dev/sda
Command: o # 创建 MBR 分区表
# 新建系统分区
Command: n
Command: w # 写入
```

#### GPT/EFI

选择以下其中一个工具操作：

```sh
$ parted /dev/sda
(parted) mklabel gpt
(parted) mkpart primary fat32 1M 100M # 用作引导分区
(parted) set 1 boot on
# 如果空间足够的话还可以分一个交换区
# mkpart primary linux-swap 100M 1.1G
(parted) mkpart primary ext4 100M -1 # 根路径
```

```sh
$ fdisk /dev/sda
Command: g # 创建 GPT 分区表
# 新建引导分区
Command: n # 新建分区
First sector:
Last sector: +512M
Command: p # 查看现状
Command: t # 更改分区类型
Partition type: l # 查看支持的类型
Partition type: 1 # EFI boot partition
# 新建系统分区
Command: n
Command: w # 写入
```

### 保留之前的数据和系统

首先要分出一块硬盘空间给 arch，这个操作我建议在已有的系统上使用图形界面完成（Windows 下的磁盘管理，KDE 的 Partition Manager 等），这样比较直观。然后：

```sh
$ fdisk /dev/sda
# 新建系统分区
Command: n
Command: w # 写入
```

## 建立文件系统

```sh
mkfs.fat -F32 /dev/sda1 # 如果空硬盘 && GPT/EFI
mkfs.ext4 /dev/sda2
```

然后挂载到路径树中，以便下一步操作。

```sh
mount /dev/sda2 /mnt
```

### 如果是 GPT/EFI

```sh
mkdir /mnt/boot
### 方式一
	mount /dev/sda1 /mnt/boot
### 方式二
	mkdir /mnt/boot/efi
	mount /dev/sda1 /mnt/boot/efi
```

这一步和 GRUB 的安装有关。两种方式 grub 看起来都是安装在 `/boot/grub` 中。不同的是：

* 第一种，grub 实际安装在引导分区中。
* 第二种，grub 实际安装在 archlinux 分区中。如果将来直接删掉 arch 的分区，电脑将无法正常启动。我采用的是第二种。

## 使能自动挂载

[fstab](https://wiki.archlinux.org/index.php/Fstab_(简体中文)) 貌似是什么自动挂载的东西，必须配置。

```sh
genfstab -U /mnt > /mnt/etc/fstab
```

## 安装系统

更改源，安装得更快。编辑 `/etc/pacman.d/mirrorlist`，去掉所有不是中国的镜像（vim 快捷键 `d/China` `dn`）。

必须要连上网才能安装，wifi 由于驱动多种多样不一定连得上。有以太网就用以太网，没有的话用手机连上 wifi 然后打开 usb tethering，`dhcpcd`。

```sh
pacstrap /mnt base
pacstrap /mnt base-devel vim # 可选
```

## 安装 GRUB

> [GRUB - ArchWiki](https://wiki.archlinux.org/index.php/GRUB)

首先把根切换为安装的系统的根，这样不用重启就可以在安装的系统上进行配置。

```sh
arch-chroot /mnt
pacman -S os-prober ntfs-3g grub
```

### MBR/BIOS

```sh
grub-install /dev/sda # 注意是整个硬盘
# grub-install --target=i386-pc /dev/sda
```

### EFI/GPT

```sh
pacman -S efibootmgr
grub-install --target=x86_64-efi \
             --efi-directory=引导分区挂载的位置 \
             --bootloader-id=grub
```

### 生成 GRUB 配置文件

自动检测硬盘上所有系统，生成配置文件。

```sh
grub-mkconfig -o /boot/grub/grub.cfg
```

> 如果报错 `warning failed to connect to lvmetad，falling back to device scanning.`，编辑 `/etc/lvm/lvm.conf`，把 `use_lvmetad = 1` 改为 0，重新运行。

这时系统已经可以正常启动了，弹出光盘，`reboot`。

> EFI 下如果还是不能启动，可以试试在 efi 启动界面中找到 `Boot Maintenance Manager` > `Boot From File` > `grub*.efi` 回车。解决方法：[EFI 文件](/note/efi-files)。

> 如果缺失 Windows 引导选项，很可能是缺少 `ntfs-3g`，安装后再运行一次 `grub-mkconfig`。

## [配置](/note/arch-postinst)

刻的盘别急着扔，下一节还要用。

