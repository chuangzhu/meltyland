```page.ini
created = 1541856275
modified = 1550327121
```

# Linux 下的 Python 相关问题

## 默认 python 为 python2？

是这样的，在 python3 出现之前，python 只有一个名称就是 `python`。[距离 Python 2 被遗弃还有一段时间](https://pythonclock.org/)，许多 Linux 发行版仍有很多程序依赖 python2，所以 **不要卸载** python2。要使用 python3 的话另外用系统包管理器安装，需要时运行 `python3` 就可以了。

**不要更改** `/usr/bin/python` 软链接的指向。如果你觉得麻烦，可以在 `~/.bashrc` 等中加入一行：

```sh
alias python=python3
```

> Arch Linux 的 python 默认为 python3，这反倒被认为是一个 bug。不过 Arch Linux 上的打包者打包时都考虑到了这个 bug，所以也不要更改。

## 没有 pip？

使用系统的包管理器安装 pip。它的包名应该为 `python-pip`，如果是 pip3 的话就是 `python3-pip`。比如 debian 下安装就是：

```sh
sudo apt install python-pip
```

**不要使用** 各种博客中流行的 `get-pip.py` 脚本。

另外也 **不要经常使用 pip**，虽说它是 pypi 推荐的包管理器。见下一节。

## 系统包管理器和 pypi 中都有某个包，我该用哪个安装？

很多常用的 python 包在系统包管理器的源中都可以找到，比如 `apt search zeep`，就会出现 `python-zeep` 和 `python3-zeep`，前者是 python2 的包，后者是 python3 的包。

首选 **使用系统的包管理器安装** 。原因如下：

* 通过 pip 安装的 python 包只有 pip 知道它的存在。而使用系统包管理器安装的 python 包，系统包管理器和 pip **都知道** 它的存在。

  如果已经用 pip 安装了一个 python 包，后面又要用系统包管理器安装一个 {依赖于 {这个 python 包} 的非 python 包}。因为系统包管理器不知道那个包的存在，所以系统包管理器会另行下载并安装那个 python 包。于是两个包的文件就会发生冲突。

* 使用系统包管理器安装的 python 包，升级系统时也会升级这些包，省去另外运行 pip 的麻烦。

* 还有就是系统包管理器可以找到很多国内的源。相对地，pypi 因为一些众所周知的原因访问速度较慢。

如果系统源中没有这个包，那么也应该这样安装 pypi 包：

```sh
pip install --user 你要安装的包
```

这样的话该包就会被安装到 `~/.local` 用户目录下，不会和用系统包管理器安装的文件冲突。然后在 `~/.bashrc` 等中加入一行：

```sh
export PATH="$PATH:$HOME/.local/bin"
```

