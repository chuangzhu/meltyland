```page.ini
created = 1537185675
modified = 1537185675
```

# EFI 文件

这是我的 dell 电脑现在的分区示意图：

```tree
/                         <- /dev/sda7
├── boot
│   ├── efi               <- /dev/sda1
│   │   └── EFI
│   │       ├── Boot
│   │       │   ├── bootx64.efi
│   │       │   └── fbx64.efi
│   │       ├── Microsoft
│   │       │   ├── Boot
│   │       │   │   ├── BCD
│   │       │   │   ├── bootmgr.efi
│   │       │   │   └── ...
│   │       │   └── ...
│   │       ├── ubuntu
│   │       │   ├── grubx64.efi
│   │       │   └── ...
│   │       └── ...
│   ├── grub
│   │   ├── grub.cfg
│   │   ├── x86_64-efi
│   │   │   ├── core.efi
│   │   │   ├── grub.efi
│   │   │   └── ...
│   │   └── ...
│   └── ...
└── ...
```

其中 `/boot/efi/EFI/Boot/bootx64.efi` ，`/boot/efi/EFI/ubuntu/grubx64.efi`和 `/boot/grub/x86_64-efi/grub.efi` 的内容是一样的。

而在 mac 虚拟机上装的 arch 是这样的：

```tree
/                        <- /dev/sda2
├── boot                 <- /dev/sda1
│   ├── EFI
│   │   └── grub
│   │       └── grubx64.efi
│   ├── grub
│   │   ├── x86_64-efi
│   │   │   ├── core.efi
│   │   │   ├── grub.efi
│   │   │   └── ...
│   │   └── ...
│   └── ...
└── ...
```

看出什么不同了吗？

## 修复

[HP UEFI doesn't boot Ubuntu automatically - Ask Ubuntu](https://askubuntu.com/questions/554690/hp-uefi-doesnt-boot-ubuntu-automatically)

把 `/boot/EFI/grub/grubx64.efi` 复制到 `/boot/EFI/BOOT/bootx64.efi` 就可以了。

```diff
 /                        <- /dev/sda2
 ├── boot                 <- /dev/sda1
 │   ├── EFI
+│   │   ├── BOOT
+│   │   │   └── bootx64.efi
 │   │   └── grub
 │   │       └── grubx64.efi
 │   ├── grub
 │   │   ├── x86_64-efi
 │   │   │   ├── core.efi
 │   │   │   ├── grub.efi
 │   │   │   └── ...
 │   │   └── ...
 │   └── ...
 └── ...
```

