```page.ini
created = 1552785918
tag = python debian pm
```

# `python-apt` 模块笔记

[我将 PacVis 移植到了 Debian 上](https://melty.land/blog/apt-vis)，使用了这个模块。和 Arch Linux 不同，Debian 的包管理器复杂许多。而我 Google 了半天，也很少找到个像样的文档。所以我公开了这篇笔记，希望能帮到大家。

更新：python-apt 的文档在 Debian 源中的 `python-apt-doc` 包中，安装后可在 `/usr/share/doc/python-apt-doc` 下找到。可运行 `python3 -m http.server -d /usr/share/doc/python-apt-doc/html` 后用浏览器打开 http://localhost:8000/ 查看。

python-apt 在 Debian 中的包名为 `python-apt` 和 `python3-apt`，分别用于 Python 2 和 Python 3。

## 低级模块

不建议基于 `apt_pkg` 这个低级模块进行开发。

### apt\_pkg.Cache

`apt_pkg.Cache()` 实例包含所有源的数据库。使用起来类似于一个 `{'name': <apt.Package>, ...}` 词典。

```python
c = apt.apt_pkg.Cache()
```

### apt\_pkg.Package

```python
p = c['python3']
```

`p` 只包含了一些基础信息，更多的信息在 `apt_pkg.Version` 对象中。

| 属性名            | 类型              | 说明                                                   |
| ----------------- | ----------------- | ------------------------------------------------------ |
| `current_state`   | `int`             | 当前状态，取值有 `apt.apt_pkg.CURSTATE_INSTALLED` 等。 |
| `current_version` | `apt_pkg.Version` | 当前的版本。                                           |
| `version_list`    | `list`            | `[<apt_pkg.Version>, ...]`                             |

### apt\_pkg.Version

```python
v = p.current_ver
```

* `depends_list_str`:
  ```python
  {'PreDepends': [[<apt_pkg.Dependency>], ...],
   'Depends': [[<apt_pkg.Dependency>], ...],
   'Suggests': [[<apt_pkg.Dependency>], ...]}
  ```

## 高级模块

### apt.cache.Cache

```python
In [50]: apt.cache.Cache()
In [50]: <apt.cache.Cache at 0x7f8a71bb7e10>
In [51]: dict(apt.cache.Cache())
Out[51]: {'package-name': <apt.package.Package>, ...}
```

可使用以下代码筛选出已安装的包：

```python
cache = apt.cache.Cache()
packages = [p for p in cache if p.is_installed]
```

### apt.package.Package

| 属性名              | 类型                | 说明                                                         |
| ------------------- | ------------------- | ------------------------------------------------------------ |
| `installed`         | `apt.cache.Version` | 当前安装的版本的包。                                         |
| `is_installed`      | `bool`              | 是否安装。                                                   |
| `is_auto_installed` | `bool`              | 若为 `True` 则该包是在安装其他包时当作依赖安装的。需要注意的是，很多作为依赖安装的包都会被 apt-mark 标记为手动（manual）安装，因此这个属性不一定可靠。 |
| `section`           | `str`               | ![aptitude-section](https://i.loli.net/2019/03/17/5c8d94ee55c46.png) |

### apt.package.Version

| 属性名            | 类型                              | 说明                                                         |
| ----------------- | --------------------------------- | ------------------------------------------------------------ |
| `dependencies`    | `[<apt.package.Dependency>, ...]` | 一个列表，其中每一项都是一个依赖。                           |
| `suggests`        | `[<apt.package.Dependency>, ...]` |                                                              |
| `version`         | `str`                             | 这个版本的包的版本号。                                       |
| `description`     | `str`                             | 长介绍。                                                     |
| `raw_description` | `str`                             | 短介绍。                                                     |
| `installed_size`  | `int`                             | 安装后的大小，单位是 kb 。                                   |
| `provides`        | `[str, ...]`                      | 如 `python3` 当前版本的该属性值为 `['python3:any', 'python3-profiler', 'python3-profiler:any']` 。 |

### apt.package.Dependency

用起来和一个列表差不多。本身不包含什么信息，信息在其中的 `apt.package.BaseDependency` 对象里面。

```python
In [72]: apt.cache.Cache()['python3'].installed.dependencies
Out[72]: 
[<Dependency: [<BaseDependency: name:'python3-minimal' relation:'=' version:'3.5.3-1' rawtype:'PreDepends'>]>,
 <Dependency: [<BaseDependency: name:'python3.5' relation:'>=' version:'3.5.3-1~' rawtype:'Depends'>]>,
 <Dependency: [<BaseDependency: name:'libpython3-stdlib' relation:'=' version:'3.5.3-1' rawtype:'Depends'>]>,
 <Dependency: [<BaseDependency: name:'dh-python' relation:'' version:'' rawtype:'Depends'>]>]
```

### apt.package.BaseDependency

| 属性名    | 类型                | 说明 |
| --------- | ------------------- | ---- |
| `version` | `apt.cache.Version` |      |
| `name`    | `str`               |      |

