```page.ini
created = 1552810200
modified = 1552810200
```

![full-view-server](https://i.loli.net/2021/10/07/NX9FOs6HAYEjMLr.png)

# AptVis: 可视化 Debian 软件包依赖关系

[Farsserfc 的 PacVis](https://farseerfc.me/pacvis.html) 是 Arch Linux 上的一个可视化软件包依赖关系的工具。我将 PacVis 移植到了 Debian 上，称之为 AptVis。

![pacvis](https://i.loli.net/2019/03/17/5c8df7e8a42ee.png)

## 这是什么？

大多数 Linux 发行版都使用软件包机制来管理系统上的各个软件，Debian 也不例外，Debian 使用一个叫做 APT 的工具来管理软件包。使用 Debian 的你一定知道这条命令：

```sh
$ sudo apt install python3
```

Linux 的一个程序往往要和许多其他程序协作才能完成一个功能。因此软件包间有一种叫做 **依赖 **的关系。使用 APT 安装一个包时，会自动安装这个包的依赖。卸载一个包后，可以用 `apt autoremove` 来卸载这些作为依赖被安装的包。

然而，一个系统用久了，我们常常忘了自己安装过什么。这个工具可以直观地呈现出各个包之间的依赖关系，方便我们：

> 1. 找出那些曾经用过而现在不需要的包
> 2. 找出那些体积大而且占地方的包
> 3. 理清系统中安装了的包之间的关系
>
> <div align="right">— https://farseerfc.me/pacvis.html</div>

## 链接

* 代码仓库： https://gitlab.com/chuangzhu/apt-vis
* 我服务器上的运行效果： https://aptvis.melty.land

## 安装和使用

安装很简单，我已经将 AptVis 打包成了 .deb 软件包。所以只需要 [在这里下载最新的软件包](https://gitlab.com/chuangzhu/apt-vis/-/releases) ，然后使用 APT 安装即可：

```sh
$ sudo apt install ~/Downloads/aptvis_0.2.7-2_all.deb
```

运行：

```sh
$ aptvis
```

然后用浏览器打开 http://localhost:8888 。

### 兼容性

AptVis 本身只直接依赖于 `python3-pkg-resources` 、 `python3-apt` 和 `python3-tornado` ，所以理论上只要有这些包的发行版都可以使用。已经测试可以用的有 Debian 9 stretch 及之后的版本，以及 Ubuntu 等等。

![aptvis-dependency](https://i.loli.net/2019/03/17/5c8ded4660232.png)

Debian 8 jessie 安装过程中会出错，我也不知道为什么。

```log
...
E: Release 'aptvis_0.2.7-2_all.deb' for 'thunderbird-l10n-ms' was not found
E: Release 'aptvis_0.2.7-2_all.deb' for 'calendar-exchange-provider' was not found
E: Release 'aptvis_0.2.7-2_all.deb' for 'waagent' was not found
E: Release 'aptvis_0.2.7-2_all.deb' for 'walinuxagent' was not found
```

## 截图

一个 Debian 10 buster 服务器：

![debian-buster-testing](https://i.loli.net/2019/03/17/5c8def6543eb2.png)

放大后：

![zoomed](https://i.loli.net/2021/10/07/ojGY3mZwRcpLbHV.png)

一个 Python Docker 容器，基于 Ubuntu：

![docker-node](https://i.loli.net/2019/03/17/5c8def65b5cae.png)

一个新的 Debian Docker 容器：

![docker-debian](https://i.loli.net/2019/03/17/5c8df041a4960.png)

## 移植细节

* APT 的 Python 接口为 `python-apt` ，我在 [这篇笔记](https://melty.land/note/python-apt) 中有详细的描述。

  Arch Linux pacman 的 Python 接口为 `pycman` 和 `pyalpm` 。Debian 的包管理机制比 Arch Linux 复杂一些。python-apt 将一个包分为多个版本，每个版本都带有不同的信息。

* Arch Linux 的 pacman 有个概念叫做包组（ `group` ），PacVis 用绿色三角形标示。不是所有包都被归类到包组里，包组的存在更像是为了解决安装 toolchain 的问题。比如 `plasma` 就是一个包组，包含了 `breeze` `kwin` `libsysguard` 等包。只需要

  ```sh
  $ sudo pacman -S plasma
  ```

  即可完整地安装 KDE Plasma。而 Debian 的 APT 没有包组这个概念，打包者解决一个 toolchain 包含多个组件的方法则更像是打一个空包，然后把这些组件当作依赖写进去。 

  我这里把绿色三角形改为标记 `section` 。Debian 的每个包都有一个 `section` ，相当于分类。

  ![green-triangle-section](https://i.loli.net/2019/03/17/5c8dfb4fc45af.png)

* Arch Linux 中有 `repo` 这个概念，比如 `core` `extra` `community` 以及 `multilib` 等等。而 AptVis 中我直接移除了 repo 相关的代码。

* PacVis 用 `python-tornado` 作为模板引擎，模板 HTML 是 `pacvis/template/index.template.html` 。

* ```
  ➜  pacvis git:(master) tree
  .
  ├── LICENSE.md
  ├── pacvis
  │   ├── favicon.ico
  │   ├── infos.py                                <--- 修改得最多的部分
  │   ├── pacvis.py                               <--- 主入口
  │   ├── static
  │   │   ├── material.deep_purple-amber.min.css  <--- 主题，我改为了 material.pink-amber.min.css
  │   │   ├── material.min.js
  │   │   ├── material.min.js.map
  │   │   ├── pacvis.css
  │   │   ├── pacvis.js
  │   │   ├── vis.min.css
  │   │   └── vis-network.min.js
  │   └── templates
  │       └── index.template.html
  ├── PKGBUILD
  ├── README.md
  └── setup.py
  ```

* PacVis 用的主题是 Material Design Lite，可以在 https://getmdl.io/customize/index.html 下载。
