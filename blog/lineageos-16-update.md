```page.ini
created = 1557648694
modified = 1557648694
```

# LineageOS 16.0 更新过程显示 `Keep System Read only`（OnePlus 5）

## 事情是这样子的

众所周知，LineageOS 是个*滚动*发行版，没有稳定版一说，你需要不断更新来保持稳定。按照往常的经验，等待大约 5 分钟，更新下载完成；只需点击安装，系统就会自动重启，进入 TWRP 恢复模式。接下来只需等待一条条 log 划过屏幕，更新就这么完成了。

我尝试更新到 5 月 9 日的 nightly build。

![Keep System Read only?](https://i.loli.net/2019/05/12/5cd7c4e46d33f.png)

我该选哪个？而不管我选哪个，弹出来的都是 TWRP 的主界面。那个可爱的 log 界面去哪里了？

![Main](https://i.loli.net/2019/05/12/5cd7c5b2166a0.png)

## TWRP

根据 [Reddit](https://www.reddit.com/r/LineageOS/comments/a0trcx/recent_problems_with_updating_nightly_builds/) 上的说法：

> u/TimSchumi
>
> It's also possible that decryption in TWRP is broken, you might want to confirm that you are on the newest version.

咱需要保证 TWRP 是最新版本的。升级 TWRP 很容易，如果你已经获取了 root 权限，只需在 Play 商店安装 TWRP 的官方应用，，设置开启 `Run with root permissions`，下载最新版本（cheeseburger 即为 OnePlus 5）然后点击这个按钮就可以了：

![FLASH TO RECOVERY](https://i.loli.net/2019/05/12/5cd7c2a951dc3.jpg)

没有 Play 商店？请自己想办法。

这时咱们再试试安装更新。

TWRP 会让你输入密码来解密分区。不要慌张，这个密码就是你的锁屏密码。

```
Running Recovery Commands...
Error: This package requires firmware version 9.0.5 or newer. Please upgrade firmware and retry!
```

我们看到 LineageOS 的更新脚本要求咱们的固件版本是 9.0.5 以上。哈？你们不考虑一下向后兼容的吗？不是，以后要升级固件先通知咱一下行不？

## 固件

人家这么要求，我们有什么办法。我之前给出的[论坛的下载链接](https://forum.xda-developers.com/showpost.php?p=76943628&postcount=8)上最新的版本是 9.0。我 Google 了半天，最后摸出了门道：直接搜索 `cheeseburger firmware` 更容易得到相关结果。OnePlus 5 的代号是 cheeseburger （OnePlus 5T 的代号是 dumpling），别忘了。

不管这样，Google 将咱们带到了这里： https://sourceforge.net/projects/cheeseburgerdumplings/files/16.0/cheeseburger/firmware/ 。[这个项目](https://sourceforge.net/projects/cheeseburgerdumplings/)专搞 OnePlus 5/5T 的系统固件。

咱们用电脑把 `firmware_9.0.5_oneplus5.zip` 下载下来。手机进入恢复模式，连接到桌面电脑上，把这个文件推到 internal storage 下：

```sh
adb push firmware_9.0.5_oneplus5.zip /sdcard/
```

`TWRP > Install > firmware_9.0.5_oneplus5.zip > Swipe to confirm Flash` ，固件就升级好了。

重启，安装更新。我们就可以看见可爱的 log 在滚动，于是更新便愉快地完成了。

![Finishing system update...](https://i.loli.net/2019/05/12/5cd7cb6362335.jpg)