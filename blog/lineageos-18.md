# 从 LineageOS 16.1 升级到 18.1

想起来我已经好久没有更新 LineageOS 了。以至于 4 月 1 日 LineageOS 突然释出基于 Android 11 R 的 18.1，我还以为是个愚人节玩笑——我 17.1 都还没来得及更新呢，怎么就突然 18.1 了？

然而 LineageOS 群里热热闹闹的聊天却告诉我这是真的。有人分享升级时遇到的问题，有人为官方没有支持的老设备编译 LineageOS。跨越两个大版本使用旧版总归让人不安心，于是连忙更新了。

LineageOS 和一些周边项目最近的变化不小：

* LineageOS 发布了 Lineage Recovery。但我 TWRP 已经用得比较熟悉了，没太大的兴趣去尝试一个新的 recovery，后文也将用 TWRP 操作。
* OpenGApps 开发没有以前那么活跃了。OpenGApps 现在仅支持到 Android 10 Q，并且网站上标注了 10.0 的支持还有些小问题。因此 LineageOS 18.1 仅支持 MindTheGapps 作为 Google Apps 的提供。
* Magisk 后端和 Magisk Manager 合并代码库了，并称 Magisk app。这是为了解决以前 Magisk 后端和 manager 常常遇到的版本不同步的问题，一方面方便作者同步地开发，另一方面也确保了用户安装的一定是对应的版本。

我的设备距离上次更新大版本以来也有着不小的变化：

* 我卸载了 LineageOS 官方的 su addon，安装了 Magisk，这样我能在拥有 root 的情况下通过 SafetyNet 检查，访问 Google Pay 和 Ingress 这些服务。
* 我通过 Magisk 安装了 EdXposed。这基本就是 Xposed，只是 Xposed 不支持 Android 8+，而 EdXposed 支持 8.0~11.0。
* 我在 EdXposed 模块上安装了 XPrivacyLua，专门用来治那些不给权限不能用的流氓软件。
* 我安装了 Shelter，将那些天天在内部储存内到处拉屎的流氓软件放了进去，眼不见心不烦。

幸运的是，虽然有这么多变化，LineageOS 升级的步骤也和原来没有太大区别。这里记录下我的升级过程。

## 下载

* LineageOS: https://download.lineageos.org/cheeseburger 下载最新的 nightly 镜像。旁边有个 recovery image，那个就是上面说的 Lineage Recovery，这里用 TWRP 就不下载了。

  中国大陆镜像: https://mirrors.ustc.edu.cn/lineageos/full/cheeseburger/

* MindTheGapps: https://androidfilehost.com/?w=files&flid=322935

  Android FileHost 的下载速度着实拉垮，因此 [Wiki](https://wiki.lineageos.org/gapps.html) 上提供了一个镜像连接: http://downloads.codefi.re/jdcteam/javelinanddart/gapps 。然而这个链接没有加密，最好用前面那个链接上的 MD5 digest 验一下下载下来的文件。

* Magisk: https://github.com/topjohnwu/Magisk/releases

  可以看到下载里只有个 `Magisk-v22.0.apk`，没有 ZIP 文件了。但这并不意味着只需要在启动后安装这个 APK 就行了，Magisk 仍然分前端后端，只是都打进这个包里而已，我们仍然需要分别安装。将 `.apk` 后缀改为 `.zip`，传到 TWRP 里，按照以往安装后端 ZIP 文件的方式安装这个文件就行了。

你可能注意到了，与之前[这篇文章](/blog/lineageos-16)相比，这里没有列出 Android 固件。这是因为 LineageOS 18.1 的镜像已经贴心地包含了最新固件。

## 升级

确保 USB debugging 打开，然后连接到电脑上，在通知中将 USB 连接模式改为文件传输。在电脑上运行

```sh
adb reboot recovery
```

即可进入 TWRP。然后将刚刚下载好的几个包传到手机上

```sh
adb push lineage-18.1-20210405-nightly-cheeseburger-signed.zip Magisk-v22.0.zip MindTheGapps-11.0.0-arm64-20210328_143848.zip /sdcard/Download/
```

在 TWRP 点击 Install，首先安装 LineageOS 18.1。然后以前有 Google Apps 就装 MindTheGapps，有 Magisk 就装 Magisk。以前安装过 OpenGApps 也不用专门去卸载，直接安装 MindTheGapps 就行了。这里只是升级，不用抹除宝贵的数据分区 `/data`。只有之前没有安装 Google Apps，现在要装 Google Apps 才需要抹除 `/data` 分区；这似乎是 setup wizard 导致的，貌似是有一些 hack 的方法可以做到不抹除。

![成功启动](https://i.loli.net/2021/04/06/zVeaJ1D7LTgNl4Y.jpg)

![XPrivacyLua 正常工作](https://i.loli.net/2021/04/06/DpyQcIZxwuf48tO.jpg)

## 体验

### 丝滑。

Android 9 Pie 时我说过 Pie 的动画过于强烈。虽然 R 的动画只增不减，但却把握好了幅度，反而做到了一种灵敏迅捷的效果。点击一个选项时画面会往外扩张、然后渐变到子选项，返回时画面往里缩。任务视图中向上拖动完成前松开，任务窗口会往回弹两下；拖动成功后，旁边的窗口移动过来填补空缺时也会往回弹。从应用返回启动器时启动器画面会从拉伸的样式往下恢复正常。

解锁时会有短暂的锁解开的动画，然后可能是我的错觉，指纹识别好像更灵敏了。

### 更多 dark theme，更多 AMOLED black。

Dark theme 下通知中心不仅控制栏，通知条目也变成深色了。电话、信息、浏览器这些 Lineage-made applications 也有深色了。启动器的所有应用抽屉现在不仅是深色，而且是纯黑了。

![信息 app 的深色主题](https://i.loli.net/2021/04/06/sYuBKU7F4N1cp8O.jpg)

### 截屏

现在截屏会在左下角显示略缩图，可以一键分享、编辑、或者舍弃。可以通过设置开启短按截屏，短按即可划定矩形区域截屏。可惜的是这个界面本身截不了图，不能发到这里。Again, 这个动画很舒服。

![部分截屏](https://i.loli.net/2021/04/06/PfULODujHzsanE8.jpg)

那么什么时候才有原生的滚动截长图呢？

### 其他

默认返回键改到左边了，我已经习惯于使用右边作为返回键，于是打开了翻转触摸按键的选项。

收到聊天软件的通知会显示一个 bubble，可以在 bubble 中快速回复。这个我还不太会玩。

![聊天气泡](https://i.loli.net/2021/04/06/Q7th5Asp1XmCBwe.jpg)

桌面的行数减少了一行，~~AOSP 在催你换更大屏幕的手机了~~。

MindTheGapps 不像 OpenGApps 那样可以选择什么 micro、nano、pico 版之类的，它只提供一个包。包含的东西比 OpenGApps 的 pico 多不少，比如我不喜欢的负一屏。