```page.ini
created = 1549543110
modified = 1549674978
```

![Screenshot_20190207_150043.png](https://i.loli.net/2019/02/07/5c5bd7b1a3a26.png)

# VirtualBox Android x86 踩坑记录

* Android x86 主页：http://www.android-x86.org/
* 下载页：https://osdn.net/projects/android-x86/releases/

## 版本

我写这篇文章时是 Feb/2019 ，谷歌 Android 已释出 9.0 Pie， Android-x86 的最新版本是 8.1 Oreo 。但我仍推荐下载 7.1 Nougat ，原因下面会说。

## 没有图形界面

Android-x86 默认没有在 Boot 选项中指定显示器，需要手动开启。

1. 插入安装 iso，重启虚拟机。选择 `Live CD - Debug mode` 。 <div id="gui-2"></div>

2. ```sh
   mkdir /boot
   mount /dev/sda1 /boot
   vi /boot/grub/menu.lst
   ```

   （如果你不会使用 `vi` ，可以参考下 [我的 vim 笔记](/note/vim) ）有一段差不多长这样：

   ```
   title Android-x86 7.1-r2
   	kernel /android-7.1-r2/kerenl quiet ...
   	initrd /android-7.1-r2/initrd.img
   ```

3. 这段的第二行，添加 `vga=ask nomodeset xforcevesa` 。

4. 弹出 iso，重启。

5. <kbd>enter</kbd> 你应该会看到这样一个界面：
   ![Screenshot_20190207_134310.png](https://i.loli.net/2019/02/07/5c5bc7147a9ca.png)

   前面的 `vga=ask` 就是显示这样一个界面的意思。在这里选择你的分辨率，输入 `Resolution` 前面的 `Mode` （一个三位的十六进制数字）。比如我要用 `1024x768x32` 就输入 `344` 。

   分辨率最后一项：

   * Android 5.x 及以上选 `32`；
   * Android 4.x 及以下选 `16`。

6. 以后每次启动都要选分辨率。如果你嫌麻烦，可以重做一遍 [2.](#gui-2) ，只不过这次 `vga` 选项不是 `ask` 了，把它改成你选的 `Mode` 的十进制表示。

## 下载不了文件

好像原生 Android 的 Chrome 下载文件时用的都是系统的下载器，就是那个只会显示 `Queued` （排队中）的下载器。如果你因为某些众所周知的原因上不了 Google Play，那么你甚至无法安装另外的下载器。

还好 Android x86 自带 `wget` ，打开终端有两种方法：

* 按 <kbd>alt</kbd> <kbd>f1</kbd> 切换到 TTY ，按 <kbd>alt</kbd> <kbd>f7</kbd> 可返回。
* 打开 `Terminal Emulator` 终端模拟器。

```sh
cd /storage/emulated/0/Download
wget 链接
```

然后在 Downloads 或 Files 中可以找到下载的文件。

不过需要注意的是，它自带的 `wget` 版本很老，不能处理 HTTPS 协议。那些强制 HTTPS 的网站上的内容下载不了。~~当然你也可以自行编译个 curl 来下载。~~如果是这样，我们可能需要先用宿主机下载，然后：

## 宿主机给虚拟机传文件

宿主机在文件的路径运行：

```sh
python3 -m http.server
```

虚拟机运行：

```sh
wget <宿主机的内网 IP>:8000/<文件名>
```

~~最简单的方法还是 QQ 。~~

## 一些应用闪退，运行不了

~~这里专指微信。~~ 一些应用包含一些 NDK 的组件（aka. so），而且只有 `armeabi` 的架构。我们的虚拟机架构是 `x86` ，所以运行不了。不过英特尔有一个叫 `Houdini` 的 native bridge 技术，它会在运行时把 ARM 的指令集转化为 x86 的指令集。开启它本来很简单，只需要在 `Settings > Android x86 options` 或 `Settings > Apps comptibility` 中 `Enable Native Bridge` 就可以了，但是有两个问题：

* Android 8.1 的 `Houdini` 尚在开发中，存在一些 bug 。有些应用 ~~再次专指微信~~ 虽然可以打开了，但是进行某些特定操作（比如登录）时会闪退。这就是为什么我 [一开始](#版本) 建议下载 7.1 。
* 这会自动下载一些东西，而其中的一些链接在中国被屏蔽了 （Android x86 8.1 专门有对中国的优化，不过因为上面的问题我不建议使用这个版本）。

### 开启 `Native Bridge`

`enable_nativebridge` 是 Android x86 自带的下载并安装 `Houdini` 的脚本。

打开终端，`su` 切换到 root 用户。

```sh
vi $(which enable_nativebridge)
```

（ [我的 vim 笔记](/note/vim) ）其中可以看到这样的内容：

```sh
if [ -z "$1" ]; then
    if [ "`uname -m`" = "x86_64" ]; then
        v=7_y
        url=http://goo.gl/SBU3is
    else
        v=7_x
        url=http://goo.gl/0IJs40
else
	v=7_z
	url=http://goo.gl/FDrxVN
fi
```

用你能想到的任何办法在宿主机上把他们下载下来，并命名为 `SBU3is` 这样。然后宿主机开一个本地服务器挂着这些文件，比如说运行：

```sh
python3 -m http.server
```

回到虚拟机，把脚本的那一段改为：

```sh
if [ -z "$1" ]; then
    if [ "`uname -m`" = "x86_64" ]; then
        v=7_y
        url=http://<宿主机 IP>:8000/SBU3is
    else
        v=7_x
        url=http://<宿主机 IP>:8000/0IJs40
else
	v=7_z
	url=http://<宿主机 IP>:8000/FDrxVN
fi
```

保存退出，确保你还是 root 用户，运行 `enable_nativebridge` 。

## 第一次开机时 `Couldn't connect to internet.`

第一次开机时会有一个 setup 界面，这个界面只认 WIFI。我试过了好几个版本，只有 `7.x` 和 `8.x` 可以跳过联网 setup 。我还没找到其他方法绕过这个界面，那就用这两个版本吧。

## 鼠标光标难以移动

Android x86 8.1 优化了这项。其他版本下鼠标光标要按住才能移动。解决方法是在 `Machine > Settings > System > Motherboard` 中找到 Pointing Device ，改为 `PS/2 Mouse` 。