```page.ini
created = 1552211921
modified = 1552743416
```

# 从 LineageOS 15.1 升级到 16.0

这篇文章记录的是一次升级而 **不是刷机** 。我的设备是 OnePlus 5，如果你用的是其他设备，请不要照做。

## 下载

Android 9.0 一加 5 固件: https://forum.xda-developers.com/showpost.php?p=76943628&postcount=8

LineageOS: http://download.lineageos.org

中国大陆镜像 http://mirrors.ustc.edu.cn/lineageos/full/

OpenGAPPs: https://opengapps.org/ , 各个版本的差异如下：

|Application Name    |pico|nano|micro|
|:-----------------:|:--:|:--:|:--:|
|             Google System Base              |  X  |  X  |  X  |
|              Google Play Store              |  X  |  X  |  X  |
|            Google Calendar Sync             |  X  |  X  | \+  |
|              Dialer Framework               |  X  |  X  |  X  |
|            Google Text-to-Speech            |  X  |  X  |  X  |
|          Google Package Installer           |  O  |  O  |  O  |
|            Google Play Services             |  X  |  X  |  X  |
|           Device Health Services            |     |  X  |  X  |
|             Face Detect Library             |     |  X  |  X  |
|                Trusted Face                 |     |  X  |  X  |
|                Google Markup                |     |  X  |  X  |
|             Google App (Search)             |     |  X  |  X  |
|            Offline Speech Files             |     |  X  |  X  |
|          Google Digital Wellbeing⁴          |     |  X  |  X  |
|              Google Calendar¹               |     |     |  O  |
|          Google Exchange Services¹          |     |     |  O  |
|                    Gmail                    |     |     |  X  |
|            Google Now Launcher²             |     |     | 4.4 |
|                 Pixel Icons                 |     |     | 7.1 |
|               Pixel Launcher²               |     |     |  O  |
|              Google Wallpapers              |     |     |  O  |

不建议安装 micro 以上的版本，他们的组件太多了，可能会与 LineageOS 的一些组件冲突。我的选择如下：

![Screenshot_20190310_164715.png](https://i.loli.net/2019/03/10/5c84ceff586f4.png)

下载后记得验一下 MD5 和 SHA256。不然下到假 ROM，亲人两行泪。

## 升级

升级过程和刷机有点像，不过不用抹除数据。

我们已经刷过 TWRP 恢复模式。为了不让手机因为充电而自启，先拔掉数据线。然后按住 <kbd>Power</kbd> <kbd>Volume Down</kbd> 即可进入 TWRP。

这时再接上数据线连接到电脑上，把下载下来的那几个包传送到手机上：

```sh
adb push /path/to/package.zip /sdcard/
```

然后在 TWRP 中点击 Install，按顺序安装 Android 9.0 固件、LineageOS 16.0 即可。如果你之前有安装过 Google Apps，也要重新安装适用于 Android 9 的 GAPPs，且必须在安装完 LineageOS 后 **第一次启动前** 安装，否则 GAPPs 会不停地闪退。

安装过程中会提示 `Unable to mount storage` 和 `Failed to mount '/data'` 。`/data` 分区储存了你的应用数据。本来如果我们这里只是升级，不需要格式化和擦除这个分区。忽略这个错误提示即可。

![unable-to-mount-storage](https://i.loli.net/2019/03/16/5c8cf8303e7ae.jpg)

## 体验

总体而言我还是更喜欢 Android 8.0 Nougat 。

### 设计风格。

Android 9.0 Pie 采用了 Material Design 2.0，列表中各个选项占用的空间变得更大，失去了很多棱角、变得很圆润。这个变化很好描述，对比一下之前和现在的 Google Search 就知道了。

![before](https://i.loli.net/2019/03/10/5c84d60c58364.png)

![now](https://i.loli.net/2019/03/10/5c84d63201723.png)

这也许是为了适应现在的全面屏发展趋势，不过我不是那么喜欢就是了。

![photo_2019-03-10_17-33-49.jpg](https://i.loli.net/2019/03/10/5c84d9f1813a6.jpg)

### 桌面所有图标都强制加上了圆形底板。

Android 8 Nougat 中也有统一形状的机制，不过需要应用开发者主动适配才行。Android 9.0 Pie 则是强制加上了圆形底板。不过我还是比较喜欢不规则图标，这让我可以通过图标的轮廓判断应用。

我翻出我曾经的华为手机截了张截图，主题是 Joe 的 New Year Party，我很喜欢这样的不规则图标。

![photo_2019-03-10_17-24-24.jpg](https://i.loli.net/2019/03/10/5c84d7cf6d975.jpg)

### 添加了更多动画。

切换菜单和应用时会有 **强烈的** 过渡动画，有时候强烈得有点幼稚。不过通知弹出的动画我还是比较喜欢的。也可能是习惯问题，适应一段时间就好了。

### 新的后台界面。

![photo_2019-03-10_17-41-17.jpg](https://i.loli.net/2019/03/10/5c84dba9adb26.jpg)
