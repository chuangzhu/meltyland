```page.ini
created = 1547368392
modified = 1560590408
redirect = /about
```

![meltland](https://i.loli.net/2019/02/10/5c5f88de8820d.png)

# Welcome to the MeltyLand!

MeltyLand 是我的个人博客，名称来自于我很喜欢的一位 P 主，はるまきごはん，的作品 [Melty Land Nightmare](https://www.bilibili.com/video/av19713490) 。

MeltyLand 的文章分为两种类型：`note` 和 `blog`，

* `note` 主要是写给我自己看的，主要是一些知识点的总结。可能十分不完善，就原谅我吧 🌚 。
* `blog` 是写给大家看的，写一些踩坑记录、好玩的东西、人生感悟、想展示出来的新项目等 🌝 。

![meltyland-with-background.png](https://i.loli.net/2019/03/16/5c8cfe5d8d825.png)

MeltyLand 的源码托管在 [GitLab](https://gitlab.com/chuangzhu/meltyland)。

<h2 id="whoami">我是谁？</h2>

我是 geneLocated，本名 Zhu Chuang。

我的昵称 geneLocated 是 ~~中二时期~~ 随便起的，后面虽然想过很多更好的昵称，不过也懒得改了。

我的头像是我艺术课选学招贴设计时做的一个作业，用 Photoshop 画的，那节课我们学了通道和自由变换。由于我对最后的成果比较满意，而当时又缺一个头像，所以就直接拿来用了。

<h2 id='contact'>联系方式</h2>

* 你可以通过邮箱与我取得联系：

  * Mail: ![mail](https://i.loli.net/2019/06/15/5d04b2e49b80b34145.png)

* 除此之外，你还可以在其他平台获取我的一些最新的消息：

  * Twitter: https://twitter.com/genelocated

  * Telegram channel: https://t.me/joinchat/AAAAAEgaWvFWhmVg44_uHQ

* 我的代码主页：

  * GitLab: https://gitlab.com/chuangzhu

  * GitHub: https://github.com/chuangzhu

<h2 id="license">版权声明 / 许可协议</h2>

Copyright ©️ Zhu Chuang, All Rights Reserved.

[![cc-by-sa-4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)](https://creativecommons.org/licenses/by-sa/4.0)

除非另有声明，MeltyLand 的所有文章均采用 `CC-BY-SA 4.0 International` 协议进行许可。

* 您可以自由地：

  * **共享** — 在任何媒介以任何形式复制、发行本作品。

  * **演绎** — 为了任何用途，修改、转换或以本作品为基础进行创作，甚至于商业目的。

* 只需遵守以下条件：

  * **署名** — 您必须给出适当的署名，提供指向本许可协议的链接，同时标明是否作了修改。且不得以任何方式暗示许可人认同您或您的使用。

  * **相同方式共享** — 如果您基于本作品进行创作，您必须使用相同的许可协议（或 [这里](https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses) 列举的其他协议）分发您贡献的作品。

  * **没有附加限制** — 您不得限制其他人做该许可协议允许的事情。

协议全文请见： https://creativecommons.org/licenses/by-sa/4.0/legalcode

<h2 id="credit">开源鸣谢</h2>

该博客使用了以下开源项目：

* [React](https://reactjs.org) ©️ Facebook Inc, is licensed under the MIT License.
* [Next.js](https://nextjs.org) ©️ ZEIT Inc, is licensed under the MIT License.
* [Material design icons](https://github.com/google/material-design-icons) ©️ Google Inc, is licensed under the Apache License 2.0.
* [Material Components for the web](https://material.io/develop/web) ©️ Google Inc, is licensed under the MIT License.
* [Marked](https://marked.js.org) ©️ Christopher Jeffery, is licensed under the MIT License.
* [Webpack](https://webpack.js.org) ©️ JS Foundation and other contributors, is licensed under the MIT License.

该博客没有直接包含以下开源项目，但我仍然想表示感谢：

* [Arch Linux](https://www.archlinux.org)
* [Termux](https://termux.com)
* [GitLab](https://gitlab.com)
