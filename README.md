# [MeltyLand](https://melty.land)

This repository contains the posts and configurations for my own blog https://melty.land.

For the blog engine, please go to [chuangzhu/nightmare](https://gitlab.com/chuangzhu/nightmare).

## Structure:

```
.
├── blog
│   ├── blog-1.md
│   └── blog-2.md
├── note
│   ├── note-1.md
│   └── note-2.md
├── about.toml
├── config.toml
└── links.toml
```

## Building

First, get the blog engine:

```sh
$ git clone https://gitlab.com/chuangzhu/nightmare /usr/lib/nightmare
```

Then `cd` into *this* repository and start the API server:

```sh
$ /usr/lib/nighmare/server.py
```

Next, setup the front-end server:

```sh
$ cd /usr/lib/nightmare
$ export APIURL=https://api.melty.land SSR_API=http://localhost:5000
$ npm run build && npm run start
```

Finally, reverse-proxy the two servers to two domains. For example, using [Caddy](https://caddyserver.com):

```caddyfile
https://melty.land {
	proxy / localhost:3000
}
https://api.melty.land {
	proxy / localhost:5000
}
```

## License

Unless otherwise specified, all original articles are licensed under a `CC-BY-SA-4.0 International License`.
